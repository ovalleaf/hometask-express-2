const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const { sequelize } = require("./models");

const jobRouter = require("./routes/jobRoutes");
const contractRouter = require("./routes/contractRoutes");
const adminRouter = require("./routes/adminRoutes");
const balancesRouter = require("./routes/balancesRoutes");

app.use(express.json());
app.use(bodyParser.json());

app.set("sequelize", sequelize);
app.set("models", sequelize.models);

// routes
app.use("/contracts", contractRouter);
app.use("/jobs", jobRouter);
app.use("/admin", adminRouter);
app.use("/balances", balancesRouter);

module.exports = app;
