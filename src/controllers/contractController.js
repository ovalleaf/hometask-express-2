const { Op } = require("sequelize");

// Get a list of contracts belonging to a user (client or contractor)
const getContracts = async (req, res) => {
  const { Contract } = req.app.get("models");

  try {
    const contracts = await Contract.findAll({
      where: {
        [Op.or]: [
          { ClientId: req.profile.id },
          { ContractorId: req.profile.id },
        ],
        status: { [Op.ne]: "terminated" },
      },
    });

    res.json(contracts);
  } catch (error) {
    console.error("Error fetching contracts:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Get a contract by ID
const getContractById = async (req, res) => {
  const { Contract } = req.app.get("models");
  const { id } = req.params;

  try {
    const contract = await Contract.findOne({
      where: { id, ClientId: req.profile.id },
    });

    if (!contract) {
      return res
        .status(404)
        .end(`Contract ${id} not found for profile ${req.profile.id}`);
    }

    res.json(contract);
  } catch (error) {
    console.error("Error fetching contract by ID:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

module.exports = {
  getContracts,
  getContractById,
};
