const { Op } = require("sequelize");

// Get the top-paid clients within a specified date range
const getTopPaidClients = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { start, end } = req.query;
  const { limit = 2 } = req.query;

  try {
    const jobs = await Job.findAll({
      where: {
        paymentDate: {
          [Op.and]: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        paid: true,
      },
    });

    const moneyPerClient = {};

    for (const job of jobs) {
      const contract = await Contract.findOne({
        where: {
          id: job.ContractId,
        },
      });

      const user = await Profile.findOne({
        where: {
          id: contract.ClientId,
        },
      });

      if (!moneyPerClient[user.id]) {
        moneyPerClient[user.id] = {
          paid: 0,
          fullName: `${user.firstName} ${user.lastName}`,
        };
      }

      moneyPerClient[user.id].paid += job.price;
    }

    const clients = Object.values(moneyPerClient);

    // Sort clients by paid amount in descending order
    clients.sort((a, b) => b.paid - a.paid);

    // Get the top-paid clients up to the specified limit
    const topPaid = clients.slice(0, limit);

    res.json(topPaid);
  } catch (error) {
    console.error("Error fetching top-paid clients:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Get the best-paying profession within a given date range
const getBestProfession = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { start, end } = req.query;

  try {
    const jobs = await Job.findAll({
      where: {
        paymentDate: {
          [Op.and]: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        paid: true,
      },
    });

    const moneyPerProfession = {};

    for (const job of jobs) {
      const contract = await Contract.findOne({
        where: {
          id: job.ContractId,
        },
      });

      const user = await Profile.findOne({
        where: {
          id: contract.ClientId,
        },
      });

      if (!moneyPerProfession[user.profession]) {
        moneyPerProfession[user.profession] = 0;
      }

      moneyPerProfession[user.profession] += job.price;
    }

    let currentMostPaid = {
      profession: null,
      amount: null,
    };

    for (const profession in moneyPerProfession) {
      if (
        !currentMostPaid.amount ||
        moneyPerProfession[profession] > currentMostPaid.amount
      ) {
        currentMostPaid = {
          profession: profession,
          amount: moneyPerProfession[profession],
        };
      }
    }

    return res.json(currentMostPaid);
  } catch (error) {
    console.error("Error retrieving data:", error);
    return res
      .status(500)
      .json({ error: "An error occurred while fetching data" });
  }
};

module.exports = {
  getTopPaidClients,
  getBestProfession,
};
