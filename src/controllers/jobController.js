const { Op } = require("sequelize");

// Get all unpaid jobs for a user (either a client or contractor) for active contracts
const getUnpaidJobs = async (req, res) => {
  const { Job, Contract } = req.app.get("models");
  const { profile } = req;

  const t = await req.app.get("sequelize").transaction();

  try {
    const contracts = await Contract.findAll({
      where: {
        [Op.or]: [{ ClientId: profile.id }, { ContractorId: profile.id }],
        status: { [Op.ne]: "terminated" },
      },
      transaction: t,
    });

    const jobs = await Job.findAll({
      where: {
        ContractId: { [Op.in]: contracts.map((c) => c.id) },
        paid: null,
      },
      transaction: t,
    });

    await t.commit();

    res.json(jobs);
  } catch (error) {
    await t.rollback();
    console.error("Error fetching unpaid jobs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Pay for a job
const payForJob = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { job_id } = req.params;
  const { profile } = req;

  const t = await req.app.get("sequelize").transaction();

  try {
    const job = await Job.findOne({
      where: {
        id: job_id,
      },
      transaction: t,
    });

    if (!job) {
      await t.rollback();
      return res.status(404).end(`Job ${job_id} not found`);
    }

    if (profile.balance < job.price) {
      await t.rollback();
      return res
        .status(404)
        .end(
          `Insufficient funds to pay for the job. It costs R${job.price}, but the user has R${profile.balance}`
        );
    }

    const contract = await Contract.findOne({
      where: {
        id: job.ContractId,
      },
      transaction: t,
    });

    await Profile.increment("balance", {
      by: job.price,
      where: {
        id: contract.ContractorId,
      },
      transaction: t,
    });

    await Profile.decrement("balance", {
      by: job.price,
      where: {
        id: profile.id,
      },
      transaction: t,
    });

    await Job.update(
      {
        paid: true,
        paymentDate: new Date().toISOString(),
      },
      {
        where: {
          id: job_id,
        },
        transaction: t,
      }
    );

    await t.commit();

    res.json(
      `R${job.price} paid for the job to contractor ${contract.ContractorId} by client ${profile.id}`
    );
  } catch (error) {
    await t.rollback();
    console.error("Error paying for job:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

module.exports = {
  getUnpaidJobs,
  payForJob,
};
