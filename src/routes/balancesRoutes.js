const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const balancesController = require("../controllers/balancesController");

const router = express.Router();

// Deposit money into the balance of a client
router.post(
    "/deposit/:userId",
    getProfile,
    balancesController.depositBalance
  );
  
module.exports = router;
