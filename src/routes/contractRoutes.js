const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const contractController = require("../controllers/contractController");

const router = express.Router();

// Get a list of contracts belonging to a user (client or contractor)
router.get("/", getProfile, contractController.getContracts);
// Get a contract belonging to a user (client or contractor) by id
router.get("/:id", getProfile, contractController.getContractById);
// Define other contract-related routes and controllers as needed

module.exports = router;
