const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const jobController = require("../controllers/jobController");

const router = express.Router();

// Get all unpaid jobs for a user (either a client or contractor) for active contracts
router.get("/unpaid", getProfile, jobController.getUnpaidJobs);

// Pay for a job
router.post("/:job_id/pay", getProfile, jobController.payForJob);


module.exports = router;
