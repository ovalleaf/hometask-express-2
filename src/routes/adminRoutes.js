const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const adminController = require("../controllers/adminController");

const router = express.Router();

// Get the top-paid clients within a specified date range
router.get(
  "/best-clients",
  getProfile,
  adminController.getTopPaidClients
);

// Define the route to get the best-paying profession
router.get(
  "/best-profession",
  getProfile,
  adminController.getBestProfession
);

// Define other profile-related routes and controllers as needed

module.exports = router;
