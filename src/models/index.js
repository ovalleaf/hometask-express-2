const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "../database.sqlite3",
});

const makeJob = require('./Job')
const makeProfile = require('./Profile')
const makeContract = require('./Contract')

const Job = makeJob(sequelize)
const Profile = makeProfile(sequelize)
const Contract = makeContract(sequelize)

Profile.hasMany(Contract, {as :'Contractor',foreignKey:'ContractorId'})
Contract.belongsTo(Profile, {as: 'Contractor'})
Profile.hasMany(Contract, {as : 'Client', foreignKey:'ClientId'})
Contract.belongsTo(Profile, {as: 'Client'})
Contract.hasMany(Job)
Job.belongsTo(Contract)

module.exports = {
    sequelize,
    Profile,
    Contract,
    Job
};